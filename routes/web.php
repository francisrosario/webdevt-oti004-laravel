<?php

use App\Http\Controllers\StudentsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/students');
});

Route::get('/home', function () {
    return view('home');
})->middleware('auth');

Auth::routes();
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/students/search', [StudentsController::class, 'search']);

Route::resource('students', StudentsController::class)->names([
    'search' => 'students.search',
    'destroy' => 'students.destroy',
    'edit' => 'students.edit',
    'index' => 'students',
    'create' => 'students.create'
])->middleware('auth');
