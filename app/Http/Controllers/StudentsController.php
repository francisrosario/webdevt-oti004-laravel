<?php

namespace App\Http\Controllers;

use App\Models\Students;
use DB;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Students::all();
        return view('index', compact('students'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $students = DB::table('students')
            ->where('first_name', 'like', request('find'))
            ->orWhere('last_name','like', request('find'))
            ->get();
        return view('index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {
            $request->validate([
                'school_id'=>'required',
                'first_name'=>'required',
                'last_name'=>'required',
                'email_address'=>'required'
            ]);
            $students = new Students([
            'school_id' => $request->get('first_name'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email_address' => $request->get('email_address'),
            'contact_number' => $request->get('contact_number'),
            'guardian_name' => $request->get('guardian_name'),
            'course' => $request->get('course')
        ]);
            $students->save();
            return redirect('/students')->with('success', 'Student details saved!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Students::find($id);
        return view('edit', compact('students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'school_id'=>'required',
            'first_name'=>'required',
            'last_name'=>'required',
            'email_address'=>'required'
        ]);
        $students = Students::find($id);
        $students->school_id =  $request->get('school_id');
        $students->first_name = $request->get('first_name');
        $students->last_name = $request->get('last_name');
        $students->email_address = $request->get('email_address');
        $students->contact_number = $request->get('contact_number');
        $students->guardian_name = $request->get('guardian_name');
        $students->course = $request->get('course');

        $students->save();
        return redirect('/students')->with('success', 'Student details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = Students::find($id);
        $students->delete();

        return redirect('/students')->with('success', 'Student deleted!');
    }
}
