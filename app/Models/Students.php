<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $fillable = [
        'school_id',
        'first_name',
        'last_name',
        'email_address',
        'contact_number',
        'guardian_name',
        'course'
    ];
    use HasFactory;
}
