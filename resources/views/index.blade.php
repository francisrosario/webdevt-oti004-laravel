@extends('base')@extends('layouts.app')
@section('main')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Students</h1>
                <div>
                    <div class="mx-auto pull-right">
                        <div class="">
                        <span class="input-group-btn mr-5 mt-1">
                            <a href="{{ route('students.create')}}"><button class="btn btn-danger input-group">Add New Student</button></a>&nbsp
                            <form action="/students/search" method="GET" role="search">
                                <div class="input-group">
                            <button class="btn btn-primary" type="submit" title="Search by Lastname / Firstnames">
                                <span class="fas fa-search">Search Database</span>
                            </button>
                                    <input type="text" class="form-control mr-2" name="find" placeholder="Search by Lastname / Firstname" id="find">
                                    <a href="/students/search" class=" mt-1">
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <table class="table table-striped">
                <thead>
                <tr>
                    <td>School ID</td>
                    <td>Name</td>
                    <td>Email Address</td>
                    <td>Contact Number</td>
                    <td>Guardian Name</td>
                    <td>Course</td>
                    <td colspan=2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr>
                        <td>{{$student->school_id}}</td>
                        <td>{{$student->first_name}} {{$student->last_name}}</td>
                        <td>{{$student->email_address}}</td>
                        <td>{{$student->contact_number}}</td>
                        <td>{{$student->guardian_name}}</td>
                        <td>{{$student->course}}</td>
                        <td>
                            <a href="{{ route('students.edit',$student->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('students.destroy', $student->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
            </div>
@endsection
