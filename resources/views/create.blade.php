@extends('base')@extends('layouts.app')@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Add a contact</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{ route('students.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="school_id">School ID:</label>
                        <input type="text" class="form-control" name="school_id"/>
                    </div>
                    <div class="form-group">
                        <label for="first_name">First Name:</label>
                        <input type="text" class="form-control" name="first_name"/>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name:</label>
                        <input type="text" class="form-control" name="last_name"/>
                    </div>
                    <div class="form-group">
                        <label for="email_address">Email Address:</label>
                        <input type="text" class="form-control" name="email_address"/>
                    </div>
                    <div class="form-group">
                        <label for="contact_number">Contact Number:</label>
                        <input type="text" class="form-control" name="contact_number"/>
                    </div>
                    <div class="form-group">
                        <label for="guardian_name">Guardian Name:</label>
                        <input type="text" class="form-control" name="guardian_name"/>
                    </div>
                    <div class="form-group">
                        <label for="course">Course:</label>
                        <input type="text" class="form-control" name="course"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Add Student</button>
                </form>
            </div>
        </div>
    </div>
@endsection
